Rails.application.routes.draw do
  get 'password_resets/create'
  get 'password_resets/edit'
  get 'password_resets/update'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get :login, to: 'sessions#new', as: 'login'
  delete :logout, to: 'sessions#destroy', as: 'logout'
  get :register, to: 'users#new', as: 'register'
  post :register, to: 'users#create'

  # External Authentication
  post "oauth/callback" => "oauths#callback"
  get "oauth/callback" => "oauths#callback" # for use with Github, Facebook
  get "oauth/:provider" => "oauths#oauth", :as => :auth_at_provider

  # Languge
  resources :languages, only: [] do
  	collection do
  		get	:english
  		get	:bahasa
  	end
  end

  scope "(:locale)" do
    resources :password_resets
	  resources	:users do
	  	member do
		    get :activate
		  end
		end
  	resources :sessions, except: [ :show ]

    resources :schedules do
      collection do
        get :redirect
        get :callback
        get :calendars
        post :events
        delete :reset
        post :import
      end
    end
    resources :events do
      member do
        # put :archive
        post :search_agenda
        put :set_date
      end
    end

    resources :invitations, only: [:create] do
      member do
        put :resent
      end
    end
	end

  if Rails.env.development?
    mount LetterOpenerWeb::Engine, at: "/letter_opener"
  end

	root 'dashboard#index'
end
