class CreateMemberships < ActiveRecord::Migration[5.1]
  def change
    create_table :memberships do |t|
      t.references :user, foreign_key: true
      t.references :event, foreign_key: true
      t.boolean	:is_admin, default: false

      t.timestamps
    end
  end
end
