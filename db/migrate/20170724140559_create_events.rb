class CreateEvents < ActiveRecord::Migration[5.1]
  def change
    create_table :events do |t|
      t.string :name
      t.text :description
      t.integer :attendance
      t.string :location
      t.datetime :start_at
      t.string :start_at_timezone
      t.datetime :end_at
      t.string :end_at_timezone

      t.timestamps
    end
  end
end
