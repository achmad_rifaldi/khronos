class CreateInvitations < ActiveRecord::Migration[5.1]
  def change
    create_table :invitations do |t|
      t.string :email
      t.references :event, foreign_key: true
      t.integer :sender_id, foreign_key: true
      t.integer :recipient_id, foreign_key: true
      t.string :token

      t.timestamps
    end
  end
end
