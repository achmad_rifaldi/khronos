class CreateSchedules < ActiveRecord::Migration[5.1]
  def change
    create_table :schedules do |t|
      t.references :user, foreign_key: true
      t.string :title
      t.datetime :start_at
      t.datetime :end_at
      t.boolean :all_day, default: false
      t.string :label

      t.timestamps
    end
  end
end
