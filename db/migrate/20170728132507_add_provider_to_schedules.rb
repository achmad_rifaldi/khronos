class AddProviderToSchedules < ActiveRecord::Migration[5.1]
  def change
    add_column :schedules, :provider, :string
  end
end
