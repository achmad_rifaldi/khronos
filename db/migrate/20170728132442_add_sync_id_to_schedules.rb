class AddSyncIdToSchedules < ActiveRecord::Migration[5.1]
  def change
    add_column :schedules, :sync_id, :string
  end
end
