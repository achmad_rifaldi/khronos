// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require material-dashboard/jquery-3.1.1.min
//= require material-dashboard/jquery-ui.min
//= require material-dashboard/bootstrap.min
//= require material-dashboard/material.min
//= require material-dashboard/perfect-scrollbar.jquery.min
//= require material-dashboard/jquery.validate.min
//= require material-dashboard/moment.min
//= require material-dashboard/chartist.min
//= require material-dashboard/jquery.bootstrap-wizard
//= require material-dashboard/bootstrap-notify
//= require material-dashboard/jquery.sharrre
//= require material-dashboard/bootstrap-datetimepicker
//= require material-dashboard/jquery-jvectormap
//= require material-dashboard/nouislider.min
//= require material-dashboard/jquery.select-bootstrap
//= require material-dashboard/jquery.datatables
//= require material-dashboard/sweetalert2
//= require material-dashboard/jasny-bootstrap.min
//= require material-dashboard/fullcalendar.min
//= require material-dashboard/jquery.tagsinput
//= require material-dashboard/material-dashboard
//= require gmaps
//= require daterangepicker
//= require rails-ujs
//= require custom
