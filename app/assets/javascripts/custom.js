khronos = {
	showNotification: function(type, messages){
    	$.notify({
        	icon: "notifications",
        	message: messages

        },{
            type: type,
            timer: 3000,
            placement: {
                from: 'top',
                align: 'right'
            }
        });
	},
    initFullCalendar: function(){
        $calendar = $('#calendar');

        today = new Date();
        y = today.getFullYear();
        m = today.getMonth();
        d = today.getDate();

        $calendar.fullCalendar({
            viewRender: function(view, element) {
                // We make sure that we activate the perfect scrollbar when the view isn't on Month
                if (view.name != 'month'){
                    $(element).find('.fc-scroller').perfectScrollbar();
                }
            },
            header: {
                left: 'title',
                center: 'month,agendaWeek,agendaDay',
                right: 'prev,next,today'
            },
            defaultDate: today,
            selectable: true,
            selectHelper: true,
            views: {
                month: { // name of view
                    titleFormat: 'MMMM YYYY'
                    // other view-specific options here
                },
                week: {
                    titleFormat: " MMMM D YYYY"
                },
                day: {
                    titleFormat: 'D MMM, YYYY'
                }
            },

            select: function(start, end) {

                // on select we show the Sweet Alert modal with an input
                swal({
                    title: 'Create an Event',
                    html: '<div class="form-group">' +
                            '<input class="form-control" placeholder="Event Title" id="input-field">' +
                        '</div>',
                    showCancelButton: true,
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false,
                    preConfirm: function () {
                        var eventData = {
                            authenticity_token: $('meta[name="csrf-token"]').attr('content'),
                            schedule: {
                                title: $('#input-field').val(),
                                start_at: start._d,
                                end_at: end._d
                            }
                        };
                        
                        return new Promise(function (resolve) {
                            if ( $('#input-field').val() ){
                                $.ajax({
                                    type: 'POST',
                                    url: window.location.hosts + '/schedules',
                                    data: eventData,
                                    dataType: "script",
                                    success: function() {
                                        if ($('#input-field').val()) {
                                            var agenda;
                                            agenda = {
                                                title: $('#input-field').val(),
                                                start: start,
                                                end: end
                                            };

                                            $calendar.fullCalendar('renderEvent', agenda, true); // stick? = true
                                        }

                                        $calendar.fullCalendar('unselect');
                                        swal.close(); 
                                    }
                                });
                            }
                        })
                    },
                    onOpen: function () {}
                }).then(function(result) {}).catch(swal.noop);;
            },
            editable: true,
            eventLimit: true, // allow "more" link when too many events


            // color classes: [ event-blue | event-azure | event-green | event-orange | event-red ]
            events: {
                url: window.location.hosts+'/schedules'
            },
            eventDrop: function(event, delta, revertFunc) {
                console.log(event);

                var endAt = event.end == null ? event.start.format() : event.end.format();

                var eventData = {
                    authenticity_token: $('meta[name="csrf-token"]').attr('content'),
                    schedule: {
                        title: event.title,
                        start_at: event.start.format(),
                        end_at: endAt
                    }
                };

                khronos.updateAgenda(event.id, eventData);

            },
            eventResize: function(event, delta, revertFunc) {
                var eventData = {
                    authenticity_token: $('meta[name="csrf-token"]').attr('content'),
                    schedule: {
                        title: event.title,
                        start_at: event.start.format(),
                        end_at: event.end.format()
                    }
                };

                khronos.updateAgenda(event.id, eventData);
            },
            eventClick: function(calEvent, jsEvent, view) {

               swal({
                  title: 'Are you sure?',
                  text: "You won't be able to revert this!",
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Yes, delete it!',
                  cancelButtonText: 'No, cancel!',
                  confirmButtonClass: 'btn btn-success',
                  cancelButtonClass: 'btn btn-danger',
                  buttonsStyling: false
                }).then(function () {
                    return new Promise(function (resolve) {
                        $.ajax({
                            type: 'DELETE',
                            url: window.location.hosts + '/schedules/' + calEvent.id,
                            dataType: "script",
                            success: function() {
                                $('#calendar').fullCalendar('removeEvents', calEvent.id);
                                swal(
                                    'Deleted!',
                                    'Your schedule has been deleted.',
                                    'success'
                                )
                            }
                        });
                    });
                }, function (dismiss) {
                  // dismiss can be 'cancel', 'overlay',
                  // 'close', and 'timer'
                  if (dismiss === 'cancel') {
                    swal(
                      'Cancelled',
                      'Your schedule is safe :)',
                      'error'
                    )
                  }
                })

            }


        });
    },
    updateAgenda: function(id, data){
        $.ajax({
            type: 'PUT',
            url: window.location.hosts + '/schedules/' + id,
            data: data,
            dataType: "script",
            success: function() {}
        });
    }
}

function setFormValidation(id) {
    $(id).validate({
        errorPlacement: function(error, element) {
            $(element).parent('div').addClass('has-error');
        }
    });
}

$(document).ready(function() {
    setFormValidation('.formValidation');

    khronos.initFullCalendar();

    $('body').on('click', '#resetSchedule', function(){
        swal({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!',
          cancelButtonText: 'No, cancel!',
          confirmButtonClass: 'btn btn-success',
          cancelButtonClass: 'btn btn-danger',
          buttonsStyling: false
        }).then(function () {
            return new Promise(function (resolve) {
                $.ajax({
                    type: 'DELETE',
                    url: window.location.hosts + '/schedules/reset',
                    dataType: "script",
                    success: function() {
                        $('#calendar').fullCalendar('removeEvents');
                        swal(
                            'Deleted!',
                            'Your schedule has been reset.',
                            'success'
                        )
                    }
                });
            });
        }, function (dismiss) {
          // dismiss can be 'cancel', 'overlay',
          // 'close', and 'timer'
          if (dismiss === 'cancel') {
            swal(
              'Cancelled',
              'Your schedule is safe :)',
              'error'
            )
          }
        })
    });
});

$(function() {

    var start = moment();
    var end = moment();

    function cb(start, end) {
        $('#start_date').val(start.format('YYYY-MM-DD'));
        $('#end_date').val(end.format('YYYY-MM-DD'));
        $('#rangedate span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    $('#rangedate').daterangepicker({
        minDate:new Date(),
        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Tomorrow': [moment().add(1, 'days'), moment().add(1, 'days')],
           '7 Days': [moment(), moment().add(6, 'days')],
           'This Month': [moment().startOf('month'), moment().endOf('month')]
        }
    }, cb);

    cb(start, end);
    
});
