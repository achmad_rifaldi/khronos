# == Schema Information
#
# Table name: events
#
#  id                :integer          not null, primary key
#  name              :string
#  description       :text
#  attendance        :integer
#  location          :string
#  start_at          :datetime
#  start_at_timezone :string
#  end_at            :datetime
#  end_at_timezone   :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class Event < ApplicationRecord
  has_many :memberships
  has_many :users, through: :memberships

  has_many :invitations

  scope :owned, -> { where("memberships.is_admin = true") }
  scope :invited, -> { where("memberships.is_admin = false") }

  def event_admin?(user)
  	memberships.where(user_id: user, is_admin: true).count > 0
  end

  def search_optimize_date(dstart, dend)
  	selected_date = dstart.to_date..dend.to_date
  	assemblage = {}

  	user_schedules = {}
  	users.each do |member|
  		user_schedules["#{member.id}"] = member.schedules.map{|schedule| (schedule.start_at.to_date..schedule.end_at.to_date).to_a}.flatten.uniq
  	end

  	selected_date.each do |ddate|
  		assemblage["#{ddate}"] = 0
  		user_schedules.each do |user_id, availability|
  			assemblage["#{ddate}"] += 1 unless availability.include?(ddate.to_date)
  		end
  	end

  	return assemblage
  end
end
