# == Schema Information
#
# Table name: schedules
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  title      :string
#  start_at   :datetime
#  end_at     :datetime
#  all_day    :boolean          default(FALSE)
#  label      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  sync_id    :string
#  provider   :string
#

class Schedule < ApplicationRecord
  belongs_to :user

  def self.between(start_time, end_time)
    where('start_at > :lo and start_at < :up',
      :lo => start_time.to_date.to_formatted_s(:db),
      :up => end_time.to_date.to_formatted_s(:db)
    )
  end

  def self.format_date(date_time)
   Time.at(date_time.to_i).to_formatted_s(:db)
  end

  def as_json(options = {})
    {
      id: self.id,
      title: self.title,
      start: start_at.rfc822,
      end: end_at.rfc822,
      allDay: all_day,
      color: "green"
    }
  end

  def parsing_ical(user, file)
    cal_file = File.open(file)
    event_items = []

    # Parser returns an array of calendars because a single file
    # can have multiple calendars.
    cals = Icalendar::Calendar.parse(cal_file)
    cals.each do |cal|
      cal.events.each do |event|
        start_at = event.dtstart
        end_at = event.dtend

        event_items << {
          user_id: user.id,
          title: event.summary,
          start_at: start_at,
          end_at: end_at,
          sync_id: event.uid,
          provider: 'google'
        } 
      end
    end

    Schedule.create(event_items)
  end
end
