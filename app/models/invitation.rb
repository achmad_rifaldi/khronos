# == Schema Information
#
# Table name: invitations
#
#  id           :integer          not null, primary key
#  email        :string
#  event_id     :integer
#  sender_id    :integer
#  recipient_id :integer
#  token        :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Invitation < ApplicationRecord
  belongs_to :event
  belongs_to :sender, class_name: 'User'
  belongs_to :recipient, class_name: 'User', optional: true
  
  before_create :generate_token
  before_save :check_user_existence

	def generate_token
	   self.token = Digest::SHA1.hexdigest([self.event_id, Time.now, rand].join)
	end

	def check_user_existence
		recipient = User.where(email: email).first
		if recipient
	  	self.recipient_id = recipient.id
		end
	end
end
