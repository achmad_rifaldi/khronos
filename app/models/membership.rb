# == Schema Information
#
# Table name: memberships
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  event_id   :integer
#  is_admin   :boolean          default(FALSE)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Membership < ApplicationRecord
  belongs_to :user
  belongs_to :event

  class << self
  	def new_member_admin(event, user)
  		member = Membership.new
  		member.event_id = event
  		member.user_id = user
  		member.is_admin = true
  		member.save
  	end

  	def new_member(event, user)
  		member = Membership.new
  		member.event_id = event
  		member.user_id = user
  		member.is_admin = false
  		member.save
  	end
	end
end
