class SessionsController < ApplicationController
	skip_before_action :require_login, except: [:destroy]

	# GET /login
  def new
  	@user = User.new
  end

  # POST /sessions
  def create
    if @user = login(params[:email], params[:password])
      redirect_back_or_to(:users, notice: 'Login successful')
    else
      flash.now[:alert] = 'Login failed'
      render action: 'new'
    end
  end

  def destroy
    logout
    redirect_to login_path, notice: 'Logged out!'
  end
end
