class InvitationsController < ApplicationController
	before_action	:set_event, only: [:create]
	before_action	:set_invitation, only: [:resent]

	def create
		is_member = @event.invitations.where(email: invitation_params[:email])
		unless is_member.blank?
			return redirect_to event_path(id: invitation_params[:event_id]), alert: "This user already invited to event."
		end

	  @invitation = Invitation.new(invitation_params)
	  @invitation.sender_id = current_user.id
	  if @invitation.save
	   		#if the user already exists
	    unless @invitation.recipient.blank?
	      #send a notification email
	      InviteMailer.existing_user_invite(@invitation).deliver

	      #Add the user to the user group
	      @invitation.recipient.events.push(@invitation.event)
	    else
	      #send the invite data to our mailer to deliver the email
	    	InviteMailer.new_user_invite(@invitation, register_url(invite_token: @invitation.token)).deliver 
	    end
	    return redirect_to event_path(id: invitation_params[:event_id]), notice: "Invitation sent successfully"
	  else
	    # oh no, creating an new invitation failed
	    redirect_to event_path(id: invitation_params[:event_id]), alert: "Invitation sent failed"
	  end
	end

	def resent
		InviteMailer.new_user_invite(@invitation, register_url(invite_token: @invitation.token)).deliver 
		redirect_to event_path(id: @invitation.event_id), notice: "Invitation sent successfully"
	end

	private

	def set_invitation
		@invitation = Invitation.find(params[:id])
	end

	def set_event
		@event = Event.find(invitation_params[:event_id])
	end

	def invitation_params
    params.require(:invitation).permit(:id, :email, :event_id, :sender_id, :recipient_id)
	end
end
