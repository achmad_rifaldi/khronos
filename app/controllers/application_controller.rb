class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_action :require_login
	before_action :set_locale
	 
	def set_locale
		I18n.locale = session[:locale] || I18n.default_locale
    session[:locale] = I18n.locale
	end

	private
	
	def not_authenticated
	  redirect_to login_path, alert: "Please login first"
	end
end
