class OauthsController < ApplicationController
	skip_before_action :require_login

  # sends the user on a trip to the provider,
  # and after authorizing there back to the callback url.
  def oauth
    login_at(auth_params[:provider])
  end

  def callback
    provider = auth_params[:provider]
    if @user = login_from(provider)
      @user.update_attribute(:remote_avatar_url, @user_hash[:user_info]["picture"])
      session[:auth_token] = @user_hash[:token]
      redirect_to root_path, notice: "Logged in from #{provider.titleize}!"
    else
      begin
        if logged_in?
          link_account(provider)
        else
          @user = create_from(provider)
          # NOTE: this is the place to add '@user.activate!' if you are using user_activation submodule
          @user.activate!
          @user.update_attribute(:remote_avatar_url, @user_hash[:user_info]["picture"])

          reset_session # protect from session fixation attack
          auto_login(@user)
        end
        
        session[:auth_token] = @user_hash[:token]
        redirect_to root_path, notice: "Logged in from #{provider.titleize}!"
      rescue
        redirect_to root_path, alert: "Failed to login from #{provider.titleize}!"
      end
    end
  end
  
  private

  def link_account(provider)
    if @user = add_provider_to_user(provider)
      # If you want to store the user's Github login, which is required in order to interact with their Github account, uncomment the next line.
      # You will also need to add a 'github_login' string column to the users table.
      #
      # @user.update_attribute(:github_login, @user_hash[:user_info]['login'])
      flash[:notice] = "You have successfully linked your Google account."
    else
      flash[:alert] = "There was a problem linking your Google account."
    end
  end

  def auth_params
    params.permit(:code, :provider)
  end
end
