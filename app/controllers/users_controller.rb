class UsersController < ApplicationController
  skip_before_action :require_login, only: [:new, :create, :activate]

  def index
  end

	# GET /register
  def new
    @token = params[:invite_token]
    if @token
      @invitation = Invitation.where(token: @token).first
      @user = User.new(email: @invitation.email)
    else
      @user = User.new
    end  
  end

  # POST /Users
  def create
    @user = User.new(user_params)
    @token = params[:invite_token]
    if @user.save
      if @token != nil
        event =  Invitation.where(token: @token).first.event #find the user group attached to the invite
        @user.events.push(event) #add this user to the new user group as a member
      end
      redirect_to login_path, notice: 'Please check your email to activate your account.'
    else
      render :new
    end
  end

  # GET /user/1
  def edit
    @user = current_user
  end

  def update
    @user = current_user
    if @user.update(user_params)
      redirect_to edit_user_path(id: current_user.id), notice: 'Profile was successfully updated.'
    else
      redirect_to edit_user_path(id: current_user.id), alert: 'Profile was failed updated.'
    end
  end

  # GET /users/1/register
  def activate
    if (@user = User.load_from_activation_token(params[:id]))
      @user.activate!
      redirect_to(login_path, notice: 'User was successfully activated.')
    else
      not_authenticated
    end
  end

	private

	# Never trust parameters from the scary internet, only allow the white list through.
  def user_params
    params.require(:user).permit(:fullname, :email, :password, :password_confirmation, :role_id, :authentications_attributes)
  end
end
