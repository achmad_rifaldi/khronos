class SchedulesController < ApplicationController
	before_action :set_schedule, only: [:show, :edit, :update, :destroy]

	def index
		@schedules = current_user.schedules.between(params['start'], params['end']) if (params['start'] && params['end']) 
		respond_to do |format| 
      format.html
      format.json { render :json => @schedules } 
    end
	end

	def create
		@schedule = Schedule.new(schedule_params)
		@schedule.user_id = current_user.id
		@schedule.save
	end

	def update
		@schedule.update_attributes(schedule_params)
	end

	def destroy
		@schedule.destroy
		respond_to do |format| 
      format.html { redirect_to schedules_path }
      format.js
    end
	end

  def reset
    current_user.schedules.destroy_all
  end

	def redirect
    client = Signet::OAuth2::Client.new({
      client_id: Rails.application.config.sorcery.google.key,
      client_secret: Rails.application.config.sorcery.google.secret,
      authorization_uri: 'https://accounts.google.com/o/oauth2/auth',
      scope: Google::Apis::CalendarV3::AUTH_CALENDAR,
      redirect_uri: callback_schedules_url
    })

    redirect_to client.authorization_uri.to_s
  end

  def callback
    client = Signet::OAuth2::Client.new({
      client_id: Rails.application.config.sorcery.google.key,
      client_secret: Rails.application.config.sorcery.google.secret,
      token_credential_uri: 'https://accounts.google.com/o/oauth2/token',
      redirect_uri: callback_schedules_url,
      code: params[:code]
    })

    response = client.fetch_access_token!

    puts response.inspect
    session[:authorization] = response

    redirect_to calendars_schedules_url
  end

  def calendars
    client = Signet::OAuth2::Client.new({
      client_id: Rails.application.config.sorcery.google.key,
      client_secret: Rails.application.config.sorcery.google.secret,
      token_credential_uri: 'https://accounts.google.com/o/oauth2/token'
    })

    client.update!(session[:authorization])

    service = Google::Apis::CalendarV3::CalendarService.new
    service.authorization = client

    @calendar_list = service.list_calendar_lists
  end

  def events
    client = Signet::OAuth2::Client.new({
      client_id: Rails.application.secrets.google_client_id,
      client_secret: Rails.application.secrets.google_client_secret,
      token_credential_uri: 'https://accounts.google.com/o/oauth2/token'
    })

    client.update!(session[:authorization])

    service = Google::Apis::CalendarV3::CalendarService.new
    service.authorization = client

    unless params[:cal_ids].blank?
    	calendar_ids = params[:cal_ids]
    	event_items = []

    	calendar_ids.each do |calendar_id|
    		@event_list = service.list_events(calendar_id)
    		@event_list.items.each do |event|
    			start_at = event.start.date_time || event.start.date
    			end_at = event.end.date_time || event.end.date

    			event_items << {
    				user_id: current_user.id,
    				title: event.summary,
    				start_at: start_at.to_datetime,
    				end_at: end_at.to_datetime,
    				sync_id: event.i_cal_uid,
    				provider: 'google'
    			} 
    		end
    	end

    	Schedule.create(event_items)
    end

    redirect_to schedules_path
  end

  def import
    
  end

	private

	def set_schedule
		@schedule = current_user.schedules.where(id: params[:id]).first
	end

	def schedule_params
		params.require(:schedule).permit(:id, :user_id, :title, :start_at, :end_at, :all_day, :label)
	end
end
