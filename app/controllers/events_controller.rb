class EventsController < ApplicationController
  before_action :set_event, only: [:show, :edit, :update, :destroy, :search_agenda, :set_date]
  def index
    @event_owned = current_user.events.owned
    @event_invited = current_user.events.invited
  end

  def show
    @invitation = Invitation.new
    @is_admin = @event.event_admin?(current_user.id)
    @event_start_at = @event.start_at.strftime("%B %d, %Y") rescue nil
  end

  def new
    @event = Event.new
  end

  def edit;end

  def create
    @event = Event.new(event_params)
    if @event.save
      Membership.new_member_admin(@event.id, current_user.id)
      redirect_to events_path, notice: 'Event was successfully created.'
    else
      render :new
    end
  end

  def update
    if @event.update(event_params)
      redirect_to events_path, notice: 'Event was successfully created.'
    else
      render :edit
    end
  end

  def destroy
    @event.destroy
    redirect_to events_path, notice: 'Event was successfully deleted.'
  end

  # def archive;end

  def search_agenda
    dstart = params[:start_date].to_date
    dend = params[:end_date].to_date
    @agendas = @event.search_optimize_date(dstart, dend)
  end

  def set_date
    @event.update_attribute(:start_at, params[:date].to_datetime)
    redirect_to event_path(id: @event.id), notice: 'Event date was successfully updated.'
  end

  private

  def set_event
    @event = Event.find(params[:id])
  end

  def event_params
    params.require(:event).permit(:id, :name, :description, :attendance, :location, :start_at, :start_at_timezone, :end_at, :end_at_timezone)
  end
end
