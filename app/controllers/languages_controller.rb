class LanguagesController < ApplicationController
	skip_before_action :require_login, only: [:english, :bahasa]

	def english
    I18n.locale = :en
    set_session_and_redirect
  end

  def bahasa
    I18n.locale = :id
    set_session_and_redirect
  end

  private

  def set_session_and_redirect
    session[:locale] = I18n.locale
    redirect_back(fallback_location: root_path)
  rescue 
    redirect_to :root
  end
end
