module ApplicationHelper
	def custom_bootstrap_flash
	  flash_messages = []
	  flash.each do |type, message|
	    type = 'success' if type == 'notice'
	    type = 'danger'   if type == 'alert'
	    text = "<script>khronos.showNotification('#{type}', '#{message}');</script>"
	    flash_messages << text.html_safe if message
	  end
	  flash_messages.join("\n").html_safe
	end

	def active_link(position)
		params[:controller] == position ? 'active' : ''
	end
end
