class InviteMailer < ApplicationMailer
	def new_user_invite(invitation, registration_uri)
		@invitation = invitation
		@event = @invitation.event
  	@url  = registration_uri

  	mail(to: invitation.email, subject: 'Event Invitation')
	end

	def existing_user_invite(invitation)
		@invitation = invitation
		@event = @invitation.event
		@url = TLD
		
		mail(to: invitation.email, subject: 'Event Invitation')
	end
end
